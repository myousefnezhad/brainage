Brain Age (BAG) Project - Preprocessing MRI files
==================================

This section explains how we can use `bag_pre_mri` command for the preprocessing step.


### Command Structure

* In order to preprocessing multiple files, `bag_pre_mri` can be run as follows:

```
bag_pre_mri <input file 1> <input file 2> <input file 3> ...
```
as an example:
```
bag_pre_mri sub3120_raw.nii.gz sub3121_raw.nii.gz sub3122_raw.nii.gz
```

* In order to preprocessing while of a directory, `bag_pre_mri` can be run as follows:
```
bag_pre_mri <some directory address>/*
```
for instance:
```
bag_pre_mri ~/brainagedata/*
```

### Outputs

This command must create a directory, where the image file and this directory have the same name, to save different steps of preprocessing. This directory includes:

1. `bet.nii.gz` as the extracted brain image in the original space
1. `flirt.nii.gz` as the brain image in the standard space, i.e., MNI 152 2mm.
1. `transformation.mat` as the transformation matrix that is generated during the registration. Note: it is a text file!
1. `fast*` as bunch of files that show different sections of brain after segmentation, i.e., white matter, gray matter, etc.


### Visualizing Outputs

You can open output files in order to check the quality of the results by using FSLeye, FSLview, or MRIcron.
