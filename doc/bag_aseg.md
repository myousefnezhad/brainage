Brain Age (BAG) Project - Segmenting MRI based on Atlas
==================================

This section explains how we can use `bag_aseg` command for the segmenting MRI files based on Reference/Atlas files


### Command Structure

* In order to segment an input file based on default references, `bag_aseg` can be run as follows:

```
bag_aseg --in <input file>
```
as an example:
```
bag_aseg --in flirt.nii.gz
```

This command will create respectively `features1.mat` and `features2.mat` for the default Atlas `HarvardOxford-cort-maxprob-thr0-2mm` and `HarvardOxford-sub-maxprob-thr0-2mm`.

* In order to segment multiple files, `bag_aseg` can be run as follows:
```
bag_aseg --in <input file 1> <input file 2> --out <output file 1> <output file 2> <output file 3> <output file 4>
```
for instance:
```
bag_aseg --in sub3120_raw/flirt.nii.gz sub3121_raw/flirt.nii.gz  --out sub3120_raw/feature1.mat sub3120_raw/feature2.mat  sub3121_raw/feature1.mat sub3121_raw/feature2.mat
```

Note: Since we have two default atlases, we need two output for each input.

* In order to segment an input file with a custom reference, `bag_aseg` can be run as follows:

```
bag_aseg --in <input file> --out <output file> --ref <ref file>
```
as an example:
```
bag_aseg --in flirt.nii.gz --out feature.mat --ref $BAG/atlas/HarvardOxford-cort-maxprob-thr0-2mm.nii.gz
```

### Outputs

This command must create MatLab files. Each file contains features (`r` + region ID) and coordinates (`c` + region ID) belong to a reference file.


### Visualizing Outputs

You can open output files by MatLab, Python, or Easy Edit (part of [Easy fMIR Project](https://easyfmri.gitlab.io/)).
