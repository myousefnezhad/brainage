Brain Age (BAG) Project
==================================

### Installing Software


### Dependencies

BAG needs following software:

  -Platform: Linux (recommended), Mac, Windows (via Linux Virtual Machine, or Bash for Windows 10 or above)

  -[Python3](https://anaconda.org/anaconda/python) (version=3.7.x)

  -[FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki) (version=5.0.1x or 6.0.0) for preprocessing steps

  -[AFNI & SUMA](https://afni.nimh.nih.gov/) (version=17.3.06 or above) for 3D visualization



## How to install Brain Age

### Pre-install for Windows 10 users:

-- Install Windows Subsystem for Linux [https://docs.microsoft.com/en-us/windows/wsl/install-win10](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

NOTE: You have to install UBUNTU Linux from Windows Store.

-- Install Xming X Server for Windows: [https://sourceforge.net/projects/xming/files/latest/download](https://sourceforge.net/projects/xming/files/latest/download)

-- Open command prompt (press Windows key + R, then in run window type `cmd` and press enter)

-- Open bash via cmd:

```
bash
```

-- Set Display:

```
echo "export DISPLAY=:0.0" >> ~/.profile
source ~/.profile
```

Now you can install easy fMRI same as a Linux system

#### STEP A) Installing 6.0.x

##### A.1) Installing FSL based on the [main source](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki)

-- Register on [FSL website](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki)

-- Download `fslinstaller.py`

-- Run following for downloading installation file (e.g. In version 6.0.1, the file name is `fsl-6.0.1-macOS_64.tar.gz` for Mac or `fsl-6.0.1-centos7_64.tar.gz` for all Linux distribution)

For Linux:
```
python2 fslinstaller.py -o
md5sum fsl-6.0.1-centos7_64.tar.gz
```

For Mac:
```
python2 fslinstaller.py -o
md5sum fsl-6.0.1-macOS_64.tar.gz
```

-- Install the downloaded file:

For Linux:
```
python2 fslinstaller.py -f fsl-6.0.1-centos7_64.tar.gz -M
export FSLDIR="/usr/local/fsl"
cd $FSLDIR/etc/fslconf
source fsl.sh
sudo ./install_fsleyes.sh
sudo ./make_applications_links.sh
sudo ./post_install.sh
```

For Mac:
```
python2 fslinstaller.py -f fsl-6.0.1-macOS_64.tar.gz -M
```

##### A.2) Installing FSL via [NeuroDebian](http://neuro.debian.net/) (For Debian or Ubuntu)

In `Get NeuroDebian` section, you must select Linux version and the closest resource location and then run the generated script. In `Select desired components`, you also must select `all software`.

For instance, this script for `Ubuntu 16.04` and `The University of Science and Technology of China (USTC)` is:
```
wget -O- http://neuro.debian.net/lists/xenial.cn-bj2.full | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
sudo apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9
sudo apt-get update
sudo apt install fsl-complete
```
and for `Ubuntu 18.04` and `The University of Science and Technology of China (USTC)` is:
```
wget -O- http://neuro.debian.net/lists/bionic.cn-bj2.full | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
sudo apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9
sudo apt-get update
sudo apt install fsl-complete
```

#### STEP B) Install AFNI 17.3.x or above

-- Download [AFNI](https://afni.nimh.nih.gov/):
* [Ubuntu 16+](https://afni.nimh.nih.gov/pub/dist/tgz/linux_ubuntu_16_64.tgz)
* [Ubuntu (<16), Fedora (< 21), Red Hat, etc.](https://afni.nimh.nih.gov/pub/dist/tgz/linux_openmp_64.tgz)
* [Fedora 21+](https://afni.nimh.nih.gov/pub/dist/tgz/linux_fedora_21_64.tgz)
* [Mac 10.7 (Lion) and higher](https://afni.nimh.nih.gov/pub/dist/tgz/macosx_10.7_Intel_64.tgz)


-- Extract AFNI to `~/abin`


##### Install linux packages for [AFNI](https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/index.html)

-- Linux, Ubuntu 15.10 and earlier: The essential system setup
```
sudo apt-get install -y tcsh libxp6 xfonts-base python-qt4             \
                        libmotif4 libmotif-dev motif-clients           \
                        gsl-bin netpbm xvfb gnome-tweak-tool           \
                        libjpeg62 xterm gedit evince git


sudo apt-get install -y tcsh xfonts-base python-qt4 gedit evince       \
                        libmotif4 libmotif-dev motif-clients           \
                        gsl-bin netpbm xvfb gnome-tweak-tool libjpeg62
```

-- Linux, Ubuntu 16.04 - 17.10: The essential system setup
```
sudo apt-get install -y tcsh xfonts-base python-qt4       \
                        gsl-bin netpbm gnome-tweak-tool   \
                        libjpeg62 xvfb xterm vim curl     \
                        gedit evince                      \
                        libglu1-mesa-dev libglw1-mesa     \
                        libxm4 build-essential git


sudo apt-get install -y gnome-terminal nautilus          \
                        gnome-icon-theme-symbolic
```

--  Linux, Ubuntu 18.04: The essential system setup
```
sudo apt-get install -y tcsh xfonts-base python-qt4       \
                        gsl-bin netpbm gnome-tweak-tool   \
                        libjpeg62 xvfb xterm vim curl     \
                        gedit evince                      \
                        libglu1-mesa-dev libglw1-mesa     \
                        libxm4 build-essential            \
                        libcurl4-openssl-dev libxml2-dev  \
                        libssl-dev libgfortran3 git


sudo apt-get install -y gnome-terminal nautilus          \
                        gnome-icon-theme-symbolic
```


-- Fedora 21 (and higher): The essential system setup
```
sudo yum install -y tcsh libXp openmotif gsl xorg-x11-fonts-misc       \
                    PyQt4 R-devel netpbm-progs gnome-tweak-tool ed     \
                    xorg-x11-server-Xvfb git
sudo yum update -y
```


-- CentOS/RHEL 7: The essential system setup
```
sudo yum install -y epel-release
sudo yum install -y tcsh libXp openmotif gsl xorg-x11-fonts-misc       \
                    PyQt4 R-devel netpbm-progs gnome-tweak-tool ed     \
                    libpng12 xorg-x11-server-Xvfb git
sudo yum update -y
```

#### STEP C) Install Python 3.7.x

-- Download [Anaconda3](https://anaconda.org/anaconda/python) for Python 3.7

* [Linux](https://www.anaconda.com/download/#linux)
* [Mac](https://www.anaconda.com/download/#macos)

Note: In Windows 10, you must install the Linux version of Anaconda 3

-- Install Python 3.7

For Linux:

```
sh Anaconda3-<version>-Linux<platform>.sh
```

For Mac: click PKG file and continue installation.



#### STEP D) Download and setup the project

-- Download Brin Age files:

From [GitLab](https://gitlab.com/myousefnezhad/brainage):

```
git clone https://gitlab.com/myousefnezhad/brainage.git  ~/.brainage
```

--- Setup the startup Script

For Linux (based on bashrc), preferred:
```
cd ~/brainage
./install_script_linux_bashrc
source ~/.startupscript
```

For Linux (based on profile), if the bashrc did not work:
```
cd ~/brainage
./install_script_linux_profile
source ~/.startupscript
```

For Mac:
```
cd ~/brainage
./install_script_mac
source ~/.startupscript
```

* Note 1: After installing the startup script, the color of command prompt must be changed! Further, the script must be automatically run, when you open a new terminal!


* Note 2: Since you did not install FSL and other related software, you may see some error on this stage such as `-bash: /usr/local/fsl/etc/fslconf/fsl.sh: No such file or directory`. Just skip them.


* Note 3: You must edit `~/.startupscript` and enable/disable different parameters based on your computer setting:
```
gedit ~/.startupscript
```
or
```
nano ~/.startupscript
```
or
```
vi ~/.startupscript
```

#### STEP E) Update and Install Python Packages


-- Update Conda Components:
```
conda update --all
```

-- Update `pip`:
```
pip install -U pip
```

-- Install `mpi4py`:
```
conda install -c conda-forge mpi4py
```

-- Install easy fMRI required python packages by using:
```
pip install -U future nibabel pyqode.core pyqode.qt
```

#### STEP F) Install Deep Learning Libraries (Optional)


-- Install [PyTorch](https://pytorch.org/) package:

For CPU version:

In Mac OS:
```
conda install pytorch torchvision -c pytorch
```

In Linux:
```
conda install pytorch-cpu torchvision-cpu -c pytorch
```

For GPU Vesion: You must install [CUDA 10](https://developer.nvidia.com/cuda-toolkit) and [cuDNN 7.4.1](https://developer.nvidia.com/cudnn). Then, you can install [PyTorch](https://pytorch.org/)

```
conda install pytorch torchvision cuda100 -c pytorch
```

## How to update Brain Age

```
bag_update
```

## How to uninstall Brain Age

```
rm -rf ~/.brainage
```
