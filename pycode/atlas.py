import sys
import os
import argparse
import nibabel as nib
import numpy as np
import scipy.io as io

# Check Input Parameters
parser = argparse.ArgumentParser()
parser.add_argument('--ref', nargs='+', help='Atlas/Reference files address (.nii or .nii.gz format)')
parser.add_argument('--in',  nargs='+', help='MRI files address (.nii or .nii.gz format)')
parser.add_argument('--out', nargs='+', help='Output files address (MatLab format)')
args = vars(parser.parse_args())
# Reading environment variable ($BAGENV)
try:
    BAGENV = os.environ["BAGENV"]
except:
    BAGENV = None
# Check Input Files
if args["in"] is None:
        print("There is no input file.")
        print("Usage:   bag_aseg --in <input file>")
        print("Example: bag_aseg --in flirt.nii.gz")
        print("")
        print("Usage:   bag_aseg --in <input file 1> <input file 2> <input file 3> --out <output file 1> <output file 2> <output file 3> ...")
        print("Example: bag_aseg --in sub3120_raw/flirt.nii.gz sub3121_raw/flirt.nii.gz  --out sub3120_raw/feature1.mat sub3120_raw/feature2.mat  sub3121_raw/feature1.mat sub3121_raw/feature2.mat")
        print("")
        print("Usage:   bag_aseg --in <input file> --out <output file> --ref <ref file>")
        print("Example: bag_aseg --in flirt.nii.gz --out feature.mat --ref $BAG/atlas/HarvardOxford-cort-maxprob-thr0-2mm.nii.gz")
        sys.exit(0)
inFiles  = args["in"]
# Check inFiles exist
for inF in inFiles:
    if not os.path.isfile(inF):
        print("Cannot find", inF)
        sys.exit(0)
    try:
        _ = nib.load(inF)
    except:
        print("Cannot load", inF)
        sys.exit(0)
# Check Ref Files
if args["ref"] is None:
    if BAGENV is None:
        print("Cannot find Brain Age environment variable ($BAGENV). Please check the ~/.startupscript")
        sys.exit(0)
    else:
        refFiles = [BAGENV + "/atlas/HarvardOxford-cort-maxprob-thr0-2mm.nii.gz", BAGENV + "/atlas/HarvardOxford-sub-maxprob-thr0-2mm.nii.gz"]
else:
    refFiles = args["ref"]
# Check refFiles exist
for refF in refFiles:
    if not os.path.isfile(refF):
        print("Cannot find", refF)
        sys.exit(0)
    try:
        _ = nib.load(refF)
    except:
        print("Cannot load", refF)
        sys.exit(0)
# Create and Check Outputs
if args["out"] is None:
     outFiles = list()
     for i in range(1, (len(refFiles) * len(inFiles)) + 1):
         outFiles.append("features" + str(i) + ".mat")
else:
    outFiles = args["out"]
if not len(outFiles) == len(refFiles) * len(inFiles):
    print("The number of output files must be", len(refFiles) * len(inFiles))
    sys.exit(0)
# Check the size of images
for refIdx, refFile in enumerate(refFiles):
    for inFile in inFiles:
        mriRe = nib.load(refFile).dataobj
        mriIn = nib.load(inFile).dataobj
        if not len(np.shape(mriRe)) == 3:
            print(refFile, "must be 3D.")
            sys.exit(0)
        if not len(np.shape(mriIn)) == 3:
            print(inFile, "must be 3D.")
            sys.exit(0)
        if not np.sum(np.array(np.shape(mriIn)) - np.array(np.shape(mriRe))) == 0:
            print("Reference: " + refFile + " " + str(np.shape(mriRe)) + " and Input: " + inFile + " " + str(np.shape(mriIn)) + " must have the same shape")
            sys.exit(0)
        print("Reference", str(refIdx + 1),  refFile, "has", np.shape(np.unique(mriRe))[0], "regions. From:", np.min(mriRe), "To:", np.max(mriRe))
# Run the results
counter = 0
for refFile in refFiles:
    for inFile in inFiles:
        mriRe = nib.load(refFile).dataobj
        mriIn = np.array(nib.load(inFile).dataobj)
        out = dict()
        for region in np.unique(mriRe):
            vector = list()
            index = np.transpose(np.where(mriRe == region))
            for (x, y, z) in index:
                vector.append(mriIn[x, y, z])
            out["r" + str(region)] = np.array(vector)
            out["c" + str(region)] = index
        io.savemat(outFiles[counter], out)
        print(outFiles[counter], "is done!")
        counter += 1
