Brain Age (BAG) Project
==================================

### Setup

-[Install Dependencies](doc/install.md)

-[Update Project](doc/upd.md)


### Commands

-[Preprocessing MRI files](doc/bag_pre_mri.md)

-[Segmenting MRI files based on a Reference/Atlas](doc/bag_aseg.md)
